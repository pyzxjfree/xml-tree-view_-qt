#include "mainview.h"

#include <QtGui/QPlastiqueStyle>
#include <QtGui/QApplication>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QStatusBar>
#include <QStringList>
#include <QDebug>

#include "XMLDom/treemodel.hpp"
#include "XMLDom/xmltreeview.hpp"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	init();
}

MainWindow::~MainWindow()
{
}

void MainWindow::init()
{    
	this->createActions();
	this->createMenus();
	this->createToolBars();
	this->createStatusBar();

	this->createxmldom();

    this->setWindowAttribute();
	this->setFontFormat();
}

void MainWindow::createActions()
{
    exitAct = new QAction(QIcon(":/images/exit.png"),tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Exit the application"));
    // connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
    connect(exitAct, SIGNAL(triggered()), qApp, SLOT(quit()));

    readAct = new QAction(QIcon(":/images/read.png"),tr("R&ead"), this);
    readAct->setShortcut(tr("Ctrl+R"));
    readAct->setStatusTip(tr("read xml file"));
    connect(readAct, SIGNAL(triggered()), this, SLOT(read()));

    saveAct = new QAction(QIcon(":/images/save.png"),tr("S&ave"), this);
    saveAct->setShortcut(tr("Ctrl+S"));
    saveAct->setStatusTip(tr("save the current modify"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    aboutAct = new QAction(QIcon(":/images/about.png"),tr("About"), this);
    aboutAct->setShortcut(tr("Ctrl+H"));
    aboutAct->setStatusTip(tr("app info"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(aboutApp())); 
}

void MainWindow::createMenus()
{
	QMenuBar *ptr_MainMenu = new QMenuBar();
    this->setMenuBar(ptr_MainMenu);

    QMenu *fileMenu = new QMenu(tr("file"));
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);
    fileMenu->addSeparator();
    fileMenu->addAction(readAct);
    fileMenu->addSeparator();
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    ptr_MainMenu->addMenu(fileMenu);

    QMenu *helpMenu = new QMenu(tr("help"));
    helpMenu->addSeparator();
    helpMenu->addAction(aboutAct);
    helpMenu->addSeparator();
    ptr_MainMenu->addMenu(helpMenu);
}

void MainWindow::createToolBars()
{
	QToolBar *ptr_Tool = new QToolBar(tr("file"),this);
    ptr_Tool->addSeparator();
    ptr_Tool->addAction(exitAct);
    ptr_Tool->addSeparator();
    ptr_Tool->addAction(readAct);
    ptr_Tool->addSeparator();
    ptr_Tool->addAction(saveAct);
    ptr_Tool->addSeparator();
    ptr_Tool->addAction(aboutAct);
    ptr_Tool->addSeparator();
    ptr_Tool->setIconSize(QSize(32,32));
    this->addToolBar(ptr_Tool);
}

void MainWindow::createStatusBar()
{
    QStatusBar *ptr_StatusBar = new QStatusBar();
    ptr_StatusBar->showMessage(tr("Ready"));
    this->setStatusBar(ptr_StatusBar);
}

void MainWindow::createxmldom()
{
	QStringList headers;
	headers << tr("Name")<<tr("Attributes")<<tr("Value");
	xmlmodel = new TreeModel(headers,TreeModel::XMLTree,this);
	xmlview = new XMLTreeView(xmlmodel,this);
	xmlview->resetColumnSize();

	setCentralWidget(xmlview);
}

void MainWindow::setWindowAttribute()
{
	QPlastiqueStyle *m_QPlastiqueStyle = new QPlastiqueStyle();
	this->setStyle(m_QPlastiqueStyle);
	delete m_QPlastiqueStyle;

	this->setWindowTitle(tr("xmlTree"));
	// this->setWindowTitle(tr("Easy Communication Electrical Power Tech. Co.,Ltd-Warning_Event_Check_Viewer."));
	this->setWindowIcon(QIcon(":/images/iconApp.png"));
	this->setMinimumSize(800, 600);
	this->setWindowState(Qt::WindowMaximized);
	this->setAutoFillBackground(true);
	this->setContentsMargins(5,0,5,0);
}

void MainWindow::setFontFormat()
{
	QFont _font("Courier New", 8);
	//QFont _font("wenquanyi", 9);
	//_font.setBold(true);
	this->setFont(_font);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QMainWindow::closeEvent(event);
}

void MainWindow::read()
{
	xmlview->read();
}

void MainWindow::save()
{
	xmlview->save();
}

void MainWindow::aboutApp()
{

}
