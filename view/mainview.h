#ifndef VIEW_MAINVIEW_H
#define VIEW_MAINVIEW_H

#include <QtGui/QMainWindow>

QT_BEGIN_NAMESPACE
class QAction;
class TreeModel;
class XMLTreeView;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();
protected:
	void closeEvent(QCloseEvent *event);
private:
	void init();
	void createActions();
	void createMenus();
    void createToolBars();
    void createStatusBar();

	void createxmldom();

	void setWindowAttribute();
	void setFontFormat();
signals:

public slots:

private slots:
	void read();
	void save();
	void aboutApp();
private:
	QAction *exitAct;
	QAction *readAct;
	QAction *saveAct;
	QAction *aboutAct;

	TreeModel *xmlmodel;
	XMLTreeView *xmlview;
};

#endif
