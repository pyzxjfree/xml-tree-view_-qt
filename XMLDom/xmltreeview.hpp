/****************************************************************************
**
** Copyright (C) 2014.
** All rights reserved.
**
****************************************************************************/
#ifndef XMLTREEVIEW_HPP
#define XMLTREEVIEW_HPP

#include <QTreeView>
#include <QDomElement>
#include <QModelIndex>
#include <QXmlStreamWriter>

QT_BEGIN_NAMESPACE
class QWidget;
class QAbstractItemModel;
class QContextMenuEvent;
QT_END_NAMESPACE

class XMLTreeView : public QTreeView
{
	Q_OBJECT
public:
    XMLTreeView ( QWidget * parent = 0 );
    XMLTreeView(QAbstractItemModel * model,QWidget * parent = 0);
    ~XMLTreeView();
protected:
    void contextMenuEvent ( QContextMenuEvent * event );
signals:
    
public slots:
    void resetColumnSize();
    void read();
    void save();
private slots:
    void insertRow();
    void removeRow();
private:
    void win_init();
    void updateActions();
    void read(QDomElement docElem);
    void write(QXmlStreamWriter &writer,const QModelIndex parent = QModelIndex());
private:

};

#endif // XMLTREEVIEW_HPP
