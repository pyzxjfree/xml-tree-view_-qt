/****************************************************************************
**
** Copyright (C) 2014.
** All rights reserved.
**
****************************************************************************/
#include "xmltreeview.hpp"

#include <string>
#include <vector>

#include <QApplication>
#include <QMenu>
#include <QAction>
#include <QColor>
#include <QPalette>
#include <QAbstractItemModel>
#include <QContextMenuEvent>
#include <QFile>
#include <QFileDialog>

#include <QDomDocument>
#include <QDebug>

#include "XMLDom/treemodel.hpp"

XMLTreeView::XMLTreeView ( QWidget * parent )
    : QTreeView(parent) 
{
    win_init();
}

XMLTreeView::XMLTreeView(QAbstractItemModel * model,QWidget * parent)
    : QTreeView(parent) 
{
    this->setModel (model);
    win_init();
}

XMLTreeView::~XMLTreeView()
{

}

void XMLTreeView::win_init()
{
    QPalette pl = this->palette();
    pl.setColor(QPalette::Base,QColor(240,240,240));
    pl.setColor(QPalette::Text,QColor(0,0,230));
    pl.setColor(QPalette::Highlight,QColor(255,255,255));
    pl.setColor(QPalette::HighlightedText,QColor(0,0,230));   
    pl.setColor(QPalette::ToolTipBase,QColor(200,200,200));
    pl.setColor(QPalette::ToolTipText,QColor(230,230,0));     
    this->setPalette(pl);

    /*
    **If mouse tracking is switched off, mouse move events only occur 
    **if a mouse button is pressed while the mouse is being moved. 
    **If mouse tracking is switched on, mouse move events occur even 
    **if no mouse button is pressed.
    **If you want to show a tooltip immediately, while the mouse is moving,
    **you must first enable mouse tracking as described above. 
    */
    // this->setMouseTracking(true);
    // this->setWordWrap(true);
    // this->setTextElideMode(Qt::ElideRight);//...display at the end of the text.
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    // this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    // this->setAlternatingRowColors(true);

    this->setStyleSheet("QTreeView::item::selected{background-color:rgba(0,230,0,127);}"
        "QTreeView::item::hover{background-color:rgba(0,230,230,127);}"
        // "QTreeView::branch{selection-color:rgba(0,230,0,127);}"
        );
}

void XMLTreeView::resetColumnSize()
{
    this->setColumnWidth(0,300);
    this->setColumnWidth(1,300);
    this->setColumnWidth(2,300);
}

void XMLTreeView::insertRow()
{
    QModelIndex index = this->selectionModel()->currentIndex();
    TreeModel *model = qobject_cast<TreeModel *>(this->model());
    QVector<QVariant> Data;
    // for(int i=0; i<model->columnCount(index);i++)
    //     Data << QObject::tr("undefine") ;
    Data << "elementName" << "id=-99" << "null";
    model->insertItemToParent(index,Data);

    this->selectionModel()->setCurrentIndex(model->index(model->rowCount(index), 0, index),
                                            QItemSelectionModel::ClearAndSelect);
    updateActions();
}

void XMLTreeView::removeRow()
{
    QModelIndex index = this->selectionModel()->currentIndex();
    QAbstractItemModel *model = this->model();
    if (model->removeRow(index.row(), index.parent()))
        updateActions();
}

void XMLTreeView::updateActions()
{
    bool hasCurrent = this->selectionModel()->currentIndex().isValid();
    if (hasCurrent) {
        this->closePersistentEditor(this->selectionModel()->currentIndex());
    }
}

void XMLTreeView::contextMenuEvent ( QContextMenuEvent * event )
{
    QMenu menu;

    if(indexAt(event->pos()).isValid()){
        QAction *removeRowAction = menu.addAction(tr("removeRow"));
        connect(removeRowAction, SIGNAL(triggered()), this, SLOT(removeRow()));
    }
    QAction *insertRowAction = menu.addAction(tr("insertRow"));
    connect(insertRowAction, SIGNAL(triggered()), this, SLOT(insertRow()));

    menu.exec(event->globalPos());

    QTreeView::contextMenuEvent(event);
}

void XMLTreeView::read()
{
    // TreeModel *model = qobject_cast<TreeModel *>(this->model());
    // QVector<QVariant> Data;

    QString filePath = QFileDialog::getOpenFileName(this, tr("Open File"),
    "./", tr("XML files (*.xml);;HTML files (*.html);;"
             "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
        QFile file(filePath);
        if (file.open(QIODevice::ReadOnly)) {
            QDomDocument document;
            if (document.setContent(&file)) {
                QDomElement docElem = document.documentElement();
                read(docElem);
            }
            file.close();
        }
    }    
}

void XMLTreeView::read(QDomElement docElem)
{
    TreeModel *model = qobject_cast<TreeModel *>(this->model());
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
            QVector<QVariant> Data;
            Data << e.tagName();
            qDebug() << e.tagName() <<"$"; // the node really is an element.
            if (e.hasAttributes())
            {
                QStringList attributes;
                for (int i = 0; i < e.attributes().count(); i++) {
                    QDomNode attribute = e.attributes().item(i);
                    attributes << attribute.nodeName() + "="
                               +attribute.nodeValue();
                 }
                QString attributeStr = attributes.join(" ");
                Data << attributeStr;
                qDebug() << attributeStr << "$"; 
            }else{
                Data << "" ;
            }
            
            if (e.hasChildNodes() && QDomNode::TextNode==n.firstChild().nodeType())
            {
                Data << n.firstChild().nodeValue();
                qDebug() << n.firstChild().nodeValue()<< "\n";
            }else{
               Data << "" ;
            }
            QVariant _parent = QVariant("");
            if (!e.parentNode().isNull())
            {
                _parent = QVariant(e.parentNode().toElement().tagName());
            }
            model->insertItemToParent(model->getModelIndex_last(_parent),Data);
            if (e.hasChildNodes())
            {
                read(e);
            }
        }
        n = n.nextSibling();
		// qDebug() <<"! "<< n.nodeType()<< " !\n";
  //       if (QDomNode::CharacterDataNode==n.nodeType())
  //       {
  //           qDebug() <<"......."<< n.nodeValue()<< "........\n";
  //           n = n.nextSibling();
  //       }
    }
}

void XMLTreeView::save()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save File"),
        "./",tr("XML files (*.xml);;HTML files (*.html);;""SVG files (*.svg);;User Interface files (*.ui)"));
    if (!filePath.isEmpty()) {
        QFile outputFile(filePath);
        if(!outputFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() << "cann't create file: " << filePath <<"\n";
            return;
        }

        QXmlStreamWriter writer(&outputFile);
        writer.setAutoFormatting(true);
        writer.setAutoFormattingIndent(2);
        writer.writeStartDocument();
        writer.writeStartElement("Define");//<Define>
        this->write(writer);
        writer.writeEndElement();//</Define>
        writer.writeEndDocument();
        outputFile.close();
    }
}

void XMLTreeView::write(QXmlStreamWriter &writer,const QModelIndex parent)
{
    TreeModel *model = qobject_cast<TreeModel *>(this->model());

    QVector<QModelIndex > childIndexs = model->findOwnerChild(parent);
    for (int i = 0; i < childIndexs.size(); i++)
    {
        QString name = model->data(model->index(childIndexs[i].row(),0,parent)).toString();
        QString atts = model->data(model->index(childIndexs[i].row(),1,parent)).toString();
        QString value = model->data(model->index(childIndexs[i].row(),2,parent)).toString();
        writer.writeStartElement(name);
        QStringList attributes = atts.split(" ",QString::SkipEmptyParts);
        for (int j = 0; j < attributes.size(); j++)
        {
            QStringList items = attributes[j].split("=",QString::SkipEmptyParts);
            if (2==items.size())
            {
                writer.writeAttribute(items[0], items[1]);
            }
        }
        if (!value.isEmpty())
        {
            writer.writeCharacters(value);
        }
        write(writer,childIndexs[i]);
        writer.writeEndElement();
    }
}