/****************************************************************************

****************************************************************************/
#include "treemodel.hpp"

#include <QtGui>
#include <QStack>
#include <QtAlgorithms>
#include <QDoubleValidator>
// #include <MLocale>
// #include <MCollator>

#include "XMLDom/treeitem.hpp"


TreeModel::TreeModel(const QStringList &headers, ModelType viewType, QObject *parent)
    : QAbstractItemModel(parent), _viewType(viewType), rootItem(NULL)
{
    resetheader(headers,viewType);
}

TreeModel::~TreeModel()
{
    delete rootItem;
    rootItem = NULL;
}

void TreeModel::resetheader(QStringList headers, ModelType viewType)
{
    _viewType = viewType;
    QVector<QVariant> rootData;
    foreach (QString header, headers)
        rootData << header;
    if (NULL!=rootItem)
    {
        delete rootItem;
        rootItem = NULL;
    }        
    rootItem = new TreeItem(rootData);
    for (int i = 0; i < rootItem->columnCount(); i++)
    {
        this->setHeaderData(i, Qt::Horizontal,rootItem->data(i),Qt::EditRole); 
    }
}

int TreeModel::columnCount(const QModelIndex & /* parent */) const
{
    return rootItem->columnCount();
}

bool TreeModel::checkSwitch(const QModelIndex _index) const
{
    if (ConnectListTable==_viewType && (this->columnCount()-1)==_index.column())
    {
        return true;
    }
    return false;
}

bool TreeModel::checkIcon(const QModelIndex _index) const
{
    return false;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole 
        && role != Qt::EditRole
        // && role != Qt::DecorationRole
        && role != Qt::CheckStateRole
        // && Qt::UserRole!=role
        // && role!=Qt::BackgroundRole
        ){
        return QVariant();
    }

    TreeItem *item = getItem(index);

    if (Qt::CheckStateRole == role){
        if (checkSwitch(index))
        {
            // qDebug() << "..check...............TreeModel::data..............\n";
            return static_cast<int>( item->getCheckFlag() ? Qt::Checked : Qt::Unchecked );
        }else{
            return QVariant();
        }
    }

    // if(Qt::DecorationRole == role ){
    //     if (checkIcon(index))
    //     {
    //         return QVariant();
    //     }else{
    //         return QVariant();
    //     }
        
    // }

    // if(role==Qt::BackgroundRole)
    // {
    //     return QVariant(item->getColor());
    // }

    if( Qt::DisplayRole == role || Qt::EditRole == role  ){
        return item->data(index.column());  
    }

    // if (Qt::UserRole==role && 0==index.column())
    // {
    //     // if (4!=item->itemtype())
    //     // {
    //     //     qDebug() << item->data(index.column()) << "::::::" << item->itemtype() << "\n";
    //     // }
        
    //     return QVariant(item->itemtype()); 
    // }
    return QVariant();
}

bool TreeModel::dropMimeData(const QMimeData *data,
    Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    qDebug() << row<<","<<column<<","<<parent.data().toString() << ",.,.,.\n";
    if (action == Qt::IgnoreAction)
        return true;

    if (!data->hasFormat("text/plain"))
        return false;

    int beginRow;

    if (row != -1)
        beginRow = row;
    else if (parent.isValid())
        beginRow = 0;
    else
        beginRow = rowCount(QModelIndex());

    QByteArray encodedData = data->data("text/plain");
    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    QHash<qint64, QMap<int,QHash<int,QString> > > newItems;

    while (!stream.atEnd()) {
        qint64 id;
        int row;
        int column;
        QString text;
        stream >> id >> row >> column >> text;
        newItems[id][row][column] = text;
    }
    int rows = newItems.count();

    insertRows(beginRow, rows, parent);
    QMap<int,QHash<int,QString> > childItems;
    foreach (childItems, newItems.values()) {
        QHash<int,QString> rowItems;
        foreach (rowItems, childItems.values()) {
            foreach (int column, rowItems.keys()) {
                QModelIndex idx = index(beginRow, column, parent);
                setData(idx, rowItems[column]);
            }
            ++beginRow;
        }
    }

    return true;
}

QMimeData *TreeModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);
    foreach (QModelIndex index, indexes) 
    {
        if (index.isValid()) {
            QString text = data(index, Qt::DisplayRole).toString();
            stream << index.internalId() << index.row() << index.column() << text;
        }
    } 
    // qDebug() << stream << "\n";
    mimeData->setData("text/plain", encodedData);
    return mimeData;
}

QStringList TreeModel::mimeTypes() const
{
    QStringList types;
    types << "text/plain";
    return types;
}

Qt::DropActions TreeModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    /*
    **if it is first colum , make it has checkbox ability
    */
    if (DVTreeTable==_viewType)
    {
        return Qt::ItemIsEnabled 
            | Qt::ItemIsSelectable
            // | Qt::ItemIsEditable
            | Qt::ItemIsDragEnabled 
            | Qt::ItemIsDropEnabled;  
    }

    if (ConnectListTable==_viewType){
        if (checkSwitch(index)){
            return Qt::ItemIsEnabled 
                | Qt::ItemIsSelectable 
                | Qt::ItemIsEditable
                | Qt::ItemIsUserCheckable; 
        }else{
            return Qt::ItemIsEnabled 
                | Qt::ItemIsSelectable 
                | Qt::ItemIsEditable; 
        }
    } 

    if (SubStructDaListTable==_viewType)
    {
        if (index.column()<5)
        {            
            return Qt::ItemIsEnabled 
                | Qt::ItemIsSelectable; 
        }else{
            return Qt::ItemIsEnabled 
                | Qt::ItemIsSelectable 
                | Qt::ItemIsEditable; 
        }
    }

    return Qt::ItemIsEnabled 
        | Qt::ItemIsSelectable
        | Qt::ItemIsEditable;
}

TreeItem *TreeModel::getItem(const QModelIndex &index) const
{
	if (!index.isValid()) 
		return rootItem;
	if(rootItem->isEmpty())
		return rootItem;

	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if (item)
		if(item->parent())	
			return item;
	return rootItem;
}


QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);
    if (orientation == Qt::Vertical && role == Qt::DisplayRole)
        return QVariant(section);    
    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();
	if(rootItem->isEmpty())
		return QModelIndex();
    TreeItem *parentItem = getItem(parent);
    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();
	if(rootItem->isEmpty())
		return QModelIndex();

    TreeItem *childItem = getItem(index);
	if (childItem == rootItem)
		return QModelIndex();

    TreeItem *parentItem = childItem->parent();
    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

bool TreeModel::insertColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;

    beginInsertColumns(parent, position, position + columns - 1);
    success = rootItem->insertColumns(position, columns);
    endInsertColumns();

    return success;
}

bool TreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    // qDebug()<<"position = "<<position<<","<<"rows = "<< rows<<"parent = "<<parent.data().toString();
    TreeItem *parentItem = getItem(parent);
    bool success;

    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows, rootItem->columnCount());
    endInsertRows();

    return success;
}

bool TreeModel::removeColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;

    beginRemoveColumns(parent, position, position + columns - 1);
    success = rootItem->removeColumns(position, columns);
    endRemoveColumns();

    if (rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    bool success = true;

    beginRemoveRows(parent, position, position + rows - 1);
    success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem = getItem(parent);

    return parentItem->childCount();
}

bool TreeModel::setData(const QModelIndex &index, 
    const QVariant &value,int role)
{
    if (role != Qt::EditRole 
        // && role != Qt::DecorationRole 
        && role != Qt::CheckStateRole
        // && role != Qt::UserRole
        // && role != Qt::BackgroundRole
        )
        return false;

    if(Qt::CheckStateRole == role ){
        if(!checkSwitch(index))//jude this item is check endable
            return false;

        bool isCheck = (value==Qt::Checked)?true:false;
        TreeItem *item = getItem(index);
            
        if(item->getCheckFlag()!=isCheck){
            if (isCheck)
            {
                QString _curType = item->data(this->columnCount()-4).toString();
                for (int i = 0; i < rowCount(); i++)
                {
                    QString _otherType = this->data(this->index(i,this->columnCount()-4)).toString();
                    if ( _curType==_otherType )
                    {
                        this->setData(this->index(i,this->columnCount()-1),Qt::Unchecked,Qt::CheckStateRole);
                    }
                }
            }
            item->setCheckFlag(isCheck);
            emit dataChanged(index, index);
        }
        return true;
    }

    // if(role==Qt::BackgroundRole)
    // {
    //     if (DVTreeTable==_viewType&&0==index.column())
    //     {
    //         TreeItem *item = getItem(index);
    //         item->setColor(value.value<QColor>());
    //         for (int i = 0; i < columnCount(); i++)
    //         {
    //             QModelIndex columnindex = this->index(index.row(),i);
    //             emit dataChanged(columnindex, columnindex);
    //         }
    //         return true;  
    //     }
    // }
    // if(Qt::DecorationRole == role ){
    //     if(!checkIcon(index))//jude this item is check endable
    //         return false;
    //     qDebug() << ",,,,,,,icon_de,,,,,,,,,,,TreeModel::setData.,,,,,,,,,,,,,,,,,,,,,\n";
    //     TreeItem *item = getItem(index);
    //     item->setData(1,value);
    //     emit dataChanged(index, index);
    //     return true;
    // }

    // if (Qt::UserRole == role )
    // {
    //     if(!checkIcon(index))//jude this item is check endable
    //         return false;
    //     qDebug() << ",,,,,,,icon_us,,,,,,,,,,,TreeModel::setData.,,,,,,,,,,,,,,,,,,,,,\n";
    //     TreeItem *item = getItem(index);
    //     item->setData(2,TreeItem *item = getItem(index););
    //     emit dataChanged(index, index);
    //     return true;       
    // }

    if (Qt::EditRole == role)
    {
		TreeItem *item = getItem(index);
		if(item!=rootItem){
            // if (!editCheck(index,value))
            // {
            //     return false;
            // }
            if(item->data(index.column()).toString()!=value.toString()){
				bool result = item->setData(index.column(), value);
                if (result)
                {
                    emit dataChanged(index, index);             
                }
				return result;
            }
		}
    }

    // if (Qt::UserRole == role && 0==index.column())
    // {
    //     TreeItem *item = getItem(index);
    //     if(item!=rootItem){
    //         if(item->itemtype()!=value.toInt()){
    //             item->setitemtype(value.toInt());
    //             return true;
    //         }
    //     }
    // }
    return false;
}

// bool TreeModel::editCheck(const QModelIndex &index, const QVariant &value)
// {
//     if (8==_viewType&&(2==index.column()||3==index.column()||4==index.column()))
//     {
//         QDoubleValidator v(-1000.0,1000.0,5);
//         int pos = 0;
//         if(v.validate(value.toString(),pos) != QValidator::Acceptable){
//             return false;
//         }
//     }
//     return true;
// }

bool TreeModel::setHeaderData(int section, Qt::Orientation orientation,
                              const QVariant &value, int role)
{
    // if (orientation==Qt::Vertical)
    // {
    //     /* code */
    // }
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    bool result = rootItem->setData(section, value);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}

void TreeModel::sort( int column, Qt::SortOrder order)
{
    if ((column < 0) || (rowCount() == 0))
        return;

    if (DVTreeTable==_viewType)
    {
        return;
    }

    if (ConnectListTable==_viewType)
    {
        return;
    }

    if (SubStructDaListTable==_viewType)
    {
        return;
    }
    // qDebug() << ",.,.,.,.,.,.,.,.,.,.\n";
    emit this->layoutAboutToBeChanged();
    // MLocale loc("zh_CN@collation=pinyin");
    // MCollator mcomp = loc.collator();
    this->sortChildren(column, order,rootItem);
    this->layoutChanged();    
    // QAbstractItemModel::sort(column,order);
}

int TreeModel::sortType(int column)
{
   int sortType = 0;
    switch(_viewType)
    {
        case DVTreeTable:
            break;
        case ConnectListTable:
            break;
        case SubStructDaListTable:        
            break;
        case GeneralTable: 
            break;
        default:
            break;
    }
    return sortType;
} 

bool TreeModel::caseInsensitiveLessThan(const QString &s1, const QString &s2,int column)
{
    int _sortType = sortType(column);
    bool ret = false;
    switch(_sortType)
    {
        case 1:
        {
            ret = (s1.toInt() < s2.toInt());
        }
            break;
        case 2:
        {
            ret = (s1.toFloat() < s2.toFloat());
        }
            break;
        case 3:
        {
            int s1_int = s1.mid(s1.indexOf("[")+1,s1.indexOf("]")-s1.indexOf("[")-1).toInt();
            int s2_int = s2.mid(s2.indexOf("[")+1,s2.indexOf("]")-s2.indexOf("[")-1).toInt();
            // qDebug() << s1_int << "," << s2_int << "\n";
            if (0==s1_int&&0==s2_int)
            {
                ret = (s1 < s2);
            }else{
               ret = (s1_int < s2_int); 
            }
            
        }
            break;
        default:
        {
            ret = (s1 < s2);
        }     
            break;                   
    }

    return ret;   
}

bool TreeModel::caseInsensitiveLessThan_S(const QString &s1, const QString &s2,int column)
{
    int _sortType = sortType(column);
    bool ret = false;
    switch(_sortType)
    {
        case 1:
        {
            ret = (s1.toInt() > s2.toInt());
        }
            break;
        case 2:
        {
            ret = (s1.toFloat() > s2.toFloat());
        }
            break;
        case 3:
        {
            int s1_int = s1.mid(s1.indexOf("[")+1,s1.indexOf("]")-s1.indexOf("[")-1).toInt();
            int s2_int = s2.mid(s2.indexOf("[")+1,s2.indexOf("]")-s2.indexOf("[")-1).toInt();
            // qDebug() << s1_int << "," << s2_int << "\n";
            if (0==s1_int&&0==s2_int)
            {
                ret = (s1 > s2);
            }else{
                ret = (s1_int > s2_int);
            }
            
        }
            break;
        default:
        {
            ret = (s1.toStdString() > s2.toStdString());
        }     
            break;                   
    }

    return ret;
}

void TreeModel::sortChildren(int column, Qt::SortOrder order,TreeItem *parentItem)
{
    // qDebug() << column << ",.,.,.,.,.,.,.,.,.,.\n";
    if (column >= parentItem->columnCount())
        return;
    for (int i = 0; i < parentItem->childCount()-1; i++)
    {
        for (int j = i; j < parentItem->childCount(); j++)
        {
            if (order == Qt::AscendingOrder)
            {
                if (caseInsensitiveLessThan_S(parentItem->child(i)->data(column).toString()
                    ,parentItem->child(j)->data(column).toString(),column))
                {
                    parentItem->swap(i,j);
                }
            }else{
                if (caseInsensitiveLessThan(parentItem->child(i)->data(column).toString()
                    ,parentItem->child(j)->data(column).toString(),column))
                {
                    parentItem->swap(i,j);
                }            
            }
        }
    }
    for (int i = 0; i < parentItem->childCount(); i++)
    {
        if (!parentItem->child(i)->isEmpty())
        {
            this->sortChildren(column,order,parentItem->child(i));
        }
    }
}

bool TreeModel::insertItem(QVector<QVariant> Data, 
    QVariant parent )
{
    bool ret = true;  
    if(QVariant()==parent){
        // qDebug()<<"parent = "<<QModelIndex().data().toString();
        insertItemToParent(QModelIndex(),Data);    
    }else{
        if(QModelIndex()==getModelIndex(parent)){
            ret = false;
        }else{
            insertItemToParent(getModelIndex(parent),Data);
        }  
    }

    return ret;
}

bool TreeModel::insertItem(QStringList &parent, 
    QVector<QVariant> Data)
{
    QModelIndex parentIndex = QModelIndex();
    for(int i=0; i<parent.size(); i++){
        QVector<QVariant> Data_t;
        Data_t<<parent.at(i);
        this->insertItemToParent(parentIndex,Data_t);
        parentIndex = this->indexChild(QVariant(parent.at(i)),parentIndex);
        if(QModelIndex()==parentIndex){
            // qDebug()<<parentIndex.data().toString();
            return false;
        }
    }

    this->insertItemToParent(parentIndex,Data);
    return true;
}

bool TreeModel::removeItem(const QVariant treeIndex)
{
    bool ret = true;
    // qDebug()<<treeIndex.toString();
    QModelIndex index = getModelIndex(treeIndex);
    
    if(index.isValid()){
        // qDebug()<<index.data().toString() <<","<<index.row();
        this->removeRow(index.row(), index.parent());
    }else{
        ret = false;
    }

    return ret;
}

void TreeModel::insertItemToParent(const QModelIndex &index, 
    QVector<QVariant> Data)
{
    // qDebug() << "123454321\n";
    if (this->columnCount(index) == 0) {
        if (!this->insertColumn(0, index))
            return;
    }
    int pos = this->rowCount(index);
    // qDebug() << "12345654321\n";
    //now jude the childs has contain the child;
    TreeItem *parentItem = getItem(index);
    if(parentItem->contains(Data[0]) 
        && (SubStructDaListTable!=_viewType&&ConnectListTable!=_viewType&&XMLTree!=_viewType)){
        // //if parent has contain it, return.
        // return;
        //this means change the child value
        int _child_pos = parentItem->indexChild(Data[0]);
        if (-1!=_child_pos)
        {
            return;
        }
    }else{
        //insert a new row
        if (!this->insertRow(pos, index))
            return;
    }
    // qDebug() << "1234567654321\n";
    //set the new row's value
    for (int column = 0; column < this->columnCount(index); column++) {
        QModelIndex child = this->index(pos, column, index);
        if( ConnectListTable==_viewType && (this->columnCount()-1)==column )
        {
            this->setData(child,Data[column], Qt::CheckStateRole);
           break;
        }
        this->setData(child,column<Data.size()?Data[column].toString():QVariant(""), Qt::EditRole);
    } 
}

//the match item maybe exist some, but return the first be finded
QModelIndex TreeModel::getModelIndex(const QVariant treeIndex)
{
    QQueue<QModelIndex> _indexQueue;
    for(int i=0; i<rowCount(); i++){
        _indexQueue.enqueue(index(i,0));
    }

    while(!_indexQueue.isEmpty()){
        if(_indexQueue.head().data().toString()==treeIndex.toString()){
            return _indexQueue.head();
        }else{
            for(int i=0; i<this->rowCount(_indexQueue.head()); i++)
                _indexQueue.enqueue(_indexQueue.head().child(i,0));
            _indexQueue.dequeue();
        }
    }

    return QModelIndex();
}

//the match item maybe exist some, but return the last be finded
QModelIndex TreeModel::getModelIndex_last(const QVariant treeIndex)
{
    QQueue<QModelIndex> _indexQueue;
    for(int i=(rowCount()-1); i>=0; i--){
        _indexQueue.enqueue(index(i,0));
    }

    while(!_indexQueue.isEmpty()){
        if(_indexQueue.head().data().toString()==treeIndex.toString()){
            return _indexQueue.head();
        }else{
            for(int i=(this->rowCount(_indexQueue.head())-1); i>=0; i--)
                _indexQueue.enqueue(_indexQueue.head().child(i,0));
            _indexQueue.dequeue();
        }
    }

    return QModelIndex();
}

QModelIndex TreeModel::indexChild(QVariant childTreeData,
      const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    TreeItem *parentItem = getItem(parent);
	int chRow = parentItem->indexChild(childTreeData);
    TreeItem *childItem = parentItem->child(chRow);
    if (childItem)
        return createIndex(chRow, 0, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::findIndex(QStringList &treeFalgs)
{
    QModelIndex index = QModelIndex();
    for(int i=0; i<treeFalgs.size(); i++){
        index = this->indexChild(treeFalgs.at(i),index);
        if(QModelIndex()==index)
            break;
    }

    return index;    
}

//find the remote parent of a design item
QModelIndex TreeModel::typeCurrentIndexParent(QModelIndex index,int &level) const
{
    QModelIndex parentIndex = index;
    level = 1;
    while(QModelIndex()!=this->parent(parentIndex)){
        parentIndex = this->parent(parentIndex);
        level+=1;
    }

    return parentIndex;
}

QVector<QModelIndex > TreeModel::findOwnerChild(const QModelIndex &parent)
{
    QVector<QModelIndex > val;
    //if (0!=parent.column())
    //{
    //    return val;
    //}
	if(QModelIndex()==parent)
	{
		for(int i=0; i<rowCount(); i++){
            val.push_back(index(i,0));
        }
	}else{
		for(int i=0; i<this->rowCount(parent); i++)
		{
			val.push_back(parent.child(i,0));
		}
	}
	return val;
}

//only column = 0 child QModelIndex be check, and by layer
QVector<QModelIndex > TreeModel::findAllChild_layer(const QModelIndex &parent )
{
    QVector<QModelIndex > val;
    QQueue<QModelIndex> _indexQueue;
    if(QModelIndex()==parent)
    {
        for(int i=0; i<rowCount(); i++){
            QModelIndex index_r = index(i,0);
            _indexQueue.enqueue(index_r);
        }
    }
    else{
        _indexQueue.enqueue(parent);
    }

    while(!_indexQueue.isEmpty()){
        val.push_back(_indexQueue.head());
        int rowC = this->rowCount(_indexQueue.head());
        if(rowC>0){
            for(int i=0; i<rowC; i++)
                _indexQueue.enqueue(_indexQueue.head().child(i,0));
        }
        _indexQueue.dequeue();
    } 
    return val;   
}

//only column = 0 child QModelIndex be check, and by order in table
QVector<QModelIndex > TreeModel::findAllChild_order(const QModelIndex &parent )
{
    QVector<QModelIndex > val;
    QStack<QModelIndex> _indexStack;
    if(QModelIndex()==parent)
    {
        for(int i=(rowCount()-1); i>=0; i--){
            QModelIndex index_r = index(i,0);
            _indexStack.push(index_r);
        }
    }else{
        _indexStack.push(parent);
    }

    while(!_indexStack.isEmpty()){
        QModelIndex index_f = _indexStack.pop();
        int rowC = this->rowCount(index_f);
        if(rowC>0){
            for(int i=(rowC-1); i>=0; i--)
                _indexStack.push(index_f.child(i,0));
        }
        val.push_back(index_f);
    } 
    return val;
}
