/****************************************************************************
****************************************************************************/

#ifndef TREEMODEL_HPP
#define TREEMODEL_HPP

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

QT_BEGIN_NAMESPACE
class TreeItem;
QT_END_NAMESPACE

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum ModelType
    {
        //
        DVTreeTable,

        //
        ConnectListTable,

        //
        SubStructDaListTable,
        
        //
        XMLTree,

        //
        GeneralTable
    };
public:
    TreeModel(const QStringList &headers, ModelType viewType = TreeModel::GeneralTable, QObject *parent = 0);  
    ~TreeModel();

    void resetheader(QStringList headers, ModelType viewType);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole);
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role = Qt::EditRole);

    void sort( int column, Qt::SortOrder order = Qt::AscendingOrder );

    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                      int row, int column, const QModelIndex &parent);
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    QStringList mimeTypes() const;
    Qt::DropActions supportedDropActions() const;
public:
    /*
    **insert a item to assign parent value,if be exit the same item, to ignore it
    **if the tree exit some items and their value equal to parent value,
    **insert new item to it's parent which is the fisrt item be finded .
    */
    bool insertItem(QVector<QVariant> Data, QVariant parent = QVariant());
    /*
    **insert a item to assign parent tree struct,
    **if be exit the same item, to ignore it ,
    **if not find the parent node, create it.
    */
    bool insertItem(QStringList &parent, QVector<QVariant> Data);
    //insert a item to assign parent,if be exit the same item, to ignore it
    void insertItemToParent(const QModelIndex &index,QVector<QVariant> Data);     
    bool removeItem(const QVariant treeIndex);  
    //check the index to assign vlaue,if be exit, return thr false find  
    QModelIndex getModelIndex(const QVariant treeIndex);
    QModelIndex getModelIndex_last(const QVariant treeIndex);
    //get the index to assign parent and child's value
    QModelIndex indexChild(QVariant childTreeData,
      const QModelIndex &parent = QModelIndex()) const; 
    QModelIndex findIndex(QStringList &treeFalgs);
    QModelIndex typeCurrentIndexParent(const QModelIndex index, int &level) const;
    QVector<QModelIndex > findOwnerChild(const QModelIndex &parent = QModelIndex());
    QVector<QModelIndex > findAllChild_layer(const QModelIndex &parent = QModelIndex());
    QVector<QModelIndex > findAllChild_order(const QModelIndex &parent = QModelIndex());
signals:
    void changeParent(QVariant _data, QVariant _pdata);
    void changeParent_error(QVariant _data, QVariant _pdata);
private:

    bool insertColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex());
    bool removeColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex());
    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex());
    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()); 
private:
    bool checkSwitch(const QModelIndex _index) const;
    bool checkIcon(const QModelIndex _index) const;
    // bool editCheck(const QModelIndex &index, const QVariant &value);
    TreeItem *getItem(const QModelIndex &index) const;
    void sortChildren(int column, Qt::SortOrder order,TreeItem *parentItem);
    int sortType(int column);
    bool caseInsensitiveLessThan(const QString &s1, const QString &s2,int column);
    bool caseInsensitiveLessThan_S(const QString &s1, const QString &s2,int column);
private:
    TreeItem *rootItem;
    ModelType _viewType;
};

#endif
