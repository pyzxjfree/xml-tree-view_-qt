#include "view/mainview.h"

#include <QtGui/QApplication>
#include <QTranslator>
#include <QTextCodec>

#pragma warning(disable:4099)

int main(int argc, char * argv[]) 
{ 
	QApplication app(argc, argv);
	QTranslator translator;
	translator.load(":/languages/xmlTreeView_cn.qm");
	app.installTranslator(&translator);

	//local string style support
	QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
	QTextCodec::setCodecForCStrings(QTextCodec::codecForLocale());
	QTextCodec::setCodecForTr(QTextCodec::codecForLocale());

	MainWindow w;
	w.show();

	return app.exec();
}
