<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN" sourcelanguage="en_HK">
<context>
    <name>DomModel</name>
    <message>
        <location filename="../XMLDom/dommodel.cpp" line="150"/>
        <source>Name</source>
        <translation type="unfinished">名称</translation>
    </message>
    <message>
        <location filename="../XMLDom/dommodel.cpp" line="152"/>
        <source>Attributes</source>
        <translation type="unfinished">属性</translation>
    </message>
    <message>
        <location filename="../XMLDom/dommodel.cpp" line="154"/>
        <source>Value</source>
        <translation type="unfinished">值</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../view/mainview.cpp" line="45"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="46"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="47"/>
        <source>Exit the application</source>
        <translation type="unfinished">退出程序</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="51"/>
        <source>S&amp;ave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="52"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="53"/>
        <source>save the current modify</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="56"/>
        <source>About</source>
        <translation type="unfinished">关于</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="57"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="58"/>
        <source>app info</source>
        <translation type="unfinished">程序信息</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="67"/>
        <location filename="../view/mainview.cpp" line="84"/>
        <source>file</source>
        <translation type="unfinished">文件</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="75"/>
        <source>help</source>
        <translation type="unfinished">帮助</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="99"/>
        <source>Ready</source>
        <translation type="unfinished">准备</translation>
    </message>
    <message>
        <location filename="../view/mainview.cpp" line="118"/>
        <source>xmlTree</source>
        <translation type="unfinished">Xml树</translation>
    </message>
</context>
<context>
    <name>XMLTreeView</name>
    <message>
        <location filename="../XMLDom/xmltreeview.cpp" line="119"/>
        <source>removeRow</source>
        <translation type="unfinished">删除行</translation>
    </message>
    <message>
        <location filename="../XMLDom/xmltreeview.cpp" line="122"/>
        <source>insertRow</source>
        <translation type="unfinished">插入行</translation>
    </message>
    <message>
        <location filename="../XMLDom/xmltreeview.cpp" line="125"/>
        <source>saveFile</source>
        <translation type="unfinished">保存文件</translation>
    </message>
</context>
</TS>
