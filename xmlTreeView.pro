QT      += core
QT      += gui
QT      += xml

CONFIG += qt warn_on debug_and_release
CONFIG += console
CONFIG += static

DESTDIR = bin
ProDir=.

INCLUDEPATH              += $$ProDir 
INCLUDEPATH              += $$ProDir/view
INCLUDEPATH              += $$ProDir/XMLDom

DEFINES +=  QTWIN_32 \
            ICE_API_EXPORTS \
            ICEE_STATIC_LIBS \
            WIN32_LEAN_AND_MEAN \
            QT_CORE_LIB \
            QT_GUI_LIB \
            QT_NETWORK_LIB \
            QT_HAVE_MMX \ 
            QT_HAVE_3DNOW \
            QT_HAVE_SSE \
            QT_HAVE_MMXEXT \
            QT_HAVE_SSE2 \
            QT_THREAD_SUPPORT \
            QT_LARGEFILE_SUPPORT \
            WIN32 \
            _UNICODE \

DEFINES -= VSWIN_32 \

CONFIG(debug, debug|release) {
    TARGET = ET-Tech_XMLTree_debug
    CONFIG += console
    OBJECTS_DIR             = debug/obj
    MOC_DIR                 = debug/moc
    DEFINES                 += _DEBUG
} else {
    TARGET = ET-Tech_XMLTree_release
    OBJECTS_DIR             = release/obj
    MOC_DIR                 = release/moc
    DEFINES                 += NODEBUG
}

HEADERS +=  view/mainview.h
HEADERS +=  XMLDom/treeitem.hpp
HEADERS +=  XMLDom/treemodel.hpp
HEADERS +=  XMLDom/xmltreeview.hpp

SOURCES +=  view/mainview.cpp 
SOURCES +=  XMLDom/treeitem.cpp
SOURCES +=  XMLDom/treemodel.cpp
SOURCES +=  XMLDom/xmltreeview.cpp

SOURCES +=  main.cpp 

RC_FILE += $$ProDir/sources/app.rc

TRANSLATIONS += $$ProDir/languages/xmlTreeView_cn.ts

RESOURCES += $$ProDir/xmlTreeView.qrc